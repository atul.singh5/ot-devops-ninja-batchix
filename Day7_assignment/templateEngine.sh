#!/bin/bash

#define parameters which are passed in.
declare -A array
echo
for key in $@
do  
    array[${key%%=*}]=${key##*=} 
cat  <<EOF >> trainer.template
         ${key%%=*} is a trainer of ${array[${key%%=*}]} 
EOF
    
done
